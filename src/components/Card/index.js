import { Link } from 'react-router-dom';
import { IoIosAddCircleOutline } from 'react-icons/io';

import { Article } from "./styles"

export default function Card(props){
    return (
        <Article>
            {props.link && 
                <Link to={`/${props.link}/${props.id}`}>
                    <IoIosAddCircleOutline />
                </Link>
            }
            <img src={props.thumbnail} alt={props.name} />
            <h2>{props.name}</h2>
        </Article>
    )
}