import styled from 'styled-components';

export const Article = styled.article`
    position: relative;
    width: 250px;
    height: 280px;
    background-color: var(--color-background);
    overflow: hidden;

    border-radius: .5rem;

    a {
        position: absolute;
        padding: 5px;
        width: 100%;
        height: 100%;
        top: 0;

        display: flex;
        justify-content: center;
        align-items: center;

        transition: .5s;
        z-index: 2;        
        
        svg {
            font-size: 3.5rem;
            transition: .5s;   
            color: transparent; ;
            border-radius: 50% 50%;

            &:hover {
                background-color: rgba(255, 0, 0, .4);
            }
        }

        &:hover {
            background: linear-gradient(to top, var(--start-color), transparent 100%);

            svg {
                color: var(--white);               
            }
        }
    }

    img {
        width: 250px;
        height: 210px;
        display: block;
        object-fit: cover;
    }

    h2 {
        color: var(--black);
        padding: 5px 10px;
        text-transform: uppercase;
        z-index: 1;
        font-size: .9rem;
    }
`;