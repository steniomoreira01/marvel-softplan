import styled from 'styled-components';

export const Content = styled.header`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    padding: 10px 15px;

    transition: .5s;
    z-index: 1000;

    &:hover {
        background-color: rgba(0, 0, 0, .6);
    }

    .content {
        max-width: 1244px;
        margin: 0 auto;

        display: flex;
        justify-content: space-between;
        align-items: center;

        img {
            max-width: 150px
        }
    }
`;