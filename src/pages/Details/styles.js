import styled from 'styled-components';

export const Main = styled.main`
    background: linear-gradient(to bottom, var(--start-color), var(--end-color));

    .topo {
        display: flex;
        justify-content: space-evenly;
        align-items: center;

        .wrapper {
            max-width: 700px;
            padding: 0 15px;
            color: var(--white);

            h1 {
                font-size: 3.5rem;
                text-transform: uppercase;
            }

            p {
                font-size:1rem;
                max-width: 700px;
                text-align:justify;
            }

            button {
                padding: 10px ;
                background: var(--yellow);
                border: 0;
                border-radius: .5rem;
                margin-top: 20px;
                cursor: pointer;
            }
        }        
    }

    .serie-list {
        background: var(--white);
        padding: 30px 15px;

        > h2 {
            position: relative;
            display: inherit;
            text-align: center;
            text-transform: uppercase;
            margin-bottom: 20px;
            color: #3c3939;

            &::before {
                position: absolute;
                content: '';
                width: 25px;
                height: 2px;
                background: red;

            }
        }

        .cards-wrappers {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-wrap: wrap;
            gap: 20px;            
        }
    }    
`;
