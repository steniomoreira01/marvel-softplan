import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { loadDetails } from '../../store/modules/character/actions';

import Header from "../../components/Header";
import Card from '../../components/Card';

import { Main } from "./styles";

export default function Details() {
    const dispatch = useDispatch();
    const { characters, characterSeries } = useSelector((state) => state.character);

    const params = useParams();

    useEffect(() => {
        dispatch(loadDetails(params.id));
    }, []);

    return (
        <>
            <Header/>
            <Main>
                <div className="topo">                
                    <img src={`${characters.path}/detail.jpg`} alt={characters.name} />

                    <div className="wrapper">                    
                        <h1>{characters.name}</h1>
                        <p>{characters.description}</p>

                        <button>To edit</button>
                    </div>
                </div>

                <div className="serie-list">
                    <h2>Series list</h2>
                    <div className="cards-wrappers">                    
                        {characterSeries.map(card => (
                            <Card
                                id={card.id}
                                name={card.title}
                                thumbnail={`${card.path}.${card.extension}`}
                            />
                        ))}
                    </div>
                </div>                
            </Main>
        </>
    )
}