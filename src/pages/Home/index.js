import { useEffect } from 'react';
import { FiSearch } from 'react-icons/fi';
import { useDispatch, useSelector } from 'react-redux';
import { loadRequest, loadSuccess } from '../../store/modules/character/actions';

import Header from "../../components/Header";
import Card from "../../components/Card";

import { Main } from "./styles";

function Home () {
    const dispatch = useDispatch();
    const { characters, characterSearch } = useSelector((state) => state.character);

    const setCharacter = (value) => {
        dispatch( loadSuccess({characterSearch: value}))
    }

    const handleOnSubmit = (event) => {
        event.preventDefault();
        dispatch(loadRequest());
    }

    useEffect(() => {
        dispatch(loadRequest());
    }, []);

    return (
        <>
            <Header/>
            <Main>
                <div className="banner">
                
                    <div className="content">
                        <h1>Explore <br/>the Universe</h1>
                        <p>
                            Dive into the dazzling domain of all the classic 
                            characters you love - and those you’ll soon discover!
                        </p>
                    </div>
                </div>

                <div className="section-cards">
                    <form onSubmit={event => handleOnSubmit(event)}>
                        <div className="input-block">
                            <FiSearch />
                            <input
                                type="text"
                                name='character'
                                placeholder="Search | Eg: Groot"
                                value={characterSearch}
                                onChange={event => setCharacter(event.target.value)}
                            />                            
                        </div>
                    </form>

                    <div className="cards-wrappers">
                        {characters.map(card => (
                            <div className="card-item" key={card.id}>                          
                                <Card
                                    id={card.id}
                                    name={card.name}
                                    link='characters'
                                    thumbnail={`${card.path}.${card.extension}`}
                                />
                            </div>                         
                        ))}
                    </div>
                </div>
            </Main>
       </>
    );
}

export default Home;