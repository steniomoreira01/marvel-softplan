import axios from 'axios';
import md5 from 'md5';

const ts = Number(new Date());

const api = axios.create({
    baseURL: process.env.REACT_APP_ENDPOINT,
    params: {   
       "ts": ts,
       "apikey": process.env.REACT_APP_PUBLIC_KEY,
       "hash": md5(`${ts}${process.env.REACT_APP_PRIVATE_KEY}${process.env.REACT_APP_PUBLIC_KEY}`)
    },
    headers: { 'Accept': '*/*' }
});

export default api;