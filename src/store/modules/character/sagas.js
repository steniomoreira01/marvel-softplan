import { all, takeLatest, call, put, select } from 'redux-saga/effects';
import { loadSuccess } from './actions';
import types from './types';
import api from '../../../services/api';

export function* charactersAll() {
    const { characterSearch } = yield select((state) => state.character);

    try {
         const response = yield call(api.get, `/characters${characterSearch && `?nameStartsWith=${characterSearch}`}`);
        
        const result = response.data.data.results.map(character => {
            return {
              id: character.id,
              name: character.name,
              path: character.thumbnail.path,
              extension: character.thumbnail.extension
            };
        });
        
        yield put(loadSuccess({characters: result}));
        
    } catch (err) {
        console.log(err.message);
    }
}

export function* characterDetails() {
    const { characterId } = yield select((state) => state.character);
    try {
        const response = yield call(api.get, `/characters/${characterId}`);        
        const result = response.data.data.results.map(character => {
            return {
                id: character.id,
                name: character.name,
                description: character.description,
                path: character.thumbnail.path,
                extension: character.thumbnail.extension
            };
        });
        yield put(loadSuccess({ characters: result[0]}));

        const seriesResponse = yield call(api.get, `/characters/${characterId}/series`);
        const seriesResult = seriesResponse.data.data.results.map(series => {
            return {
                id: series.id,
                title: series.title,
                path: series.thumbnail.path,
                extension: series.thumbnail.extension
            };
        });

        yield put(loadSuccess({ characterSeries: seriesResult}));
        
    } catch (err) {
        console.log(err.message);
    }
}

export default all([
    takeLatest(types.LOAD_REQUEST, charactersAll),
    takeLatest(types.LOAD_DETAILS, characterDetails)
]);