const types = {
    LOAD_REQUEST: '@characters/LOAD_REQUEST',
    LOAD_SUCCESS: '@characters/LOAD_SUCCESS',
    LOAD_DETAILS: '@characters/LOAD_DETAILS'
}

export default types;