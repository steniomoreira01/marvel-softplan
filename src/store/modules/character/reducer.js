import types from "./types";
import produce from 'immer';

const INITIAL_STATE = {
    characterId: '',
    characterSearch: '',
    characters: [],
    characterSeries: []
}

function character(state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.LOAD_SUCCESS: {
            return produce(state, (draft) => {
                draft = { ...draft, ...action.payload };
                return draft;
            })
        }
        case types.LOAD_DETAILS: {
            return produce(state, (draft) => {
                draft.characterId = action.characterId;
            })
        }

        default: {
            return state;
        }
    }
}

export default character;