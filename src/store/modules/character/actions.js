import types from './types';

export function loadRequest() {
    return { type: types.LOAD_REQUEST }
}

export function loadSuccess(payload) {
    return { type: types.LOAD_SUCCESS, payload }
}

export function loadDetails(characterId) {
    return { type: types.LOAD_DETAILS, characterId }
}