import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from "./pages/Home";
import Details from "./pages/Details";

export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path='/' exact component={ Home } />
                <Route path='/characters/:id' exact component={ Details } />
            </Switch>
        </BrowserRouter>
    );
}